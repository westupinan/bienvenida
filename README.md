# README

1. Instalar Node.js versión 12.0.0 > node -v
   Node Package Manager (nos sirve para instalar módulos js) > npm -v

2. Instala el módulo expo-cli de manera global (g) , se puede usar desde cualquier directorio

   > npm install -g expo-cli  
   > expo --version

3. Crear un proyecto de React Native llamado Bienvenida usando expo
   --template es para indicar el template que vamos a usar, en nuestro caso el template se llama blank

   > expo init Bienvenida --template blank

4. Levantar el proyecto: ingresar al proyecto, por ejemplo cd Bienvenida
   > expo start
   > En el browser elegir LAN o Tunel. Lan si el dispositivo y la compu están en la misma red
   > Túnel si el teléfono está en otra red.
   > Desde el teléfono escanear el código QR para visualizar la aplicación
